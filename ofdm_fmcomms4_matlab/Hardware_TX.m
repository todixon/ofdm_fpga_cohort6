clear;close all;clc;j=1i;
Global_Parameters;

%% TX Load
load('TX_signal'); % [1x972]
% transmitRepeat Mode
TX_Hardware = 2048*repmat(TX_signal.',34,1); % Transmit Data must be >= 4096 % [4860x1]

%% Hardware Parameters
Ready_Time = 0;
scale = 1024;

Mode='step'; % Select Mode
%tx_object = sdrtx('ZedBoard and FMCOMMS2/3/4', ...
%           'IPAddress',            '192.168.0.34', ...
%           'CenterFrequency',      Parameters_struct.CenterFrequency, ...
%           'BasebandSampleRate',   Parameters_struct.Bandwidth, ...  % Bandwidth
%           'Gain',                 0, ...
%           'ChannelMapping',       1);
%          'EnableBurstMode',1,...
%Len = size(TX_Hardware);
Len = 32768;
s = iio_sys_obj_matlab; % Constructor
s.ip_address = '192.168.0.35';
s.dev_name = 'ad9361';
s.in_ch_no = 2;
s.out_ch_no = 2;
s.in_ch_size = Len;
s.out_ch_size = Len;

s = s.setupImpl();

input = cell(1, s.in_ch_no + length(s.iio_dev_cfg.cfg_ch));
RFFreq = 4e9;
RFGain = 64;
RFBw = 18e6;
Fs = 30e6;
Ts = 1/Fs;
Fc = 2e6;
Len = s.in_ch_size;
t = Ts:Ts:Ts*Len;

input{s.in_ch_no+1} = RFFreq;
input{s.in_ch_no+2} = Fs;   % RX_SAMPLING_FREQ
input{s.in_ch_no+3} = RFBw;  % RX_RF_BANDWIDTH
input{s.in_ch_no+4} = 'manual';
input{s.in_ch_no+5} = RFGain;
input{s.in_ch_no+6} = RFFreq;
input{s.in_ch_no+7} = Fs;  % TX_SAMPLING_FREQ
input{s.in_ch_no+8} = RFBw;  % TX_RF_BANDWIDTH

output = cell(1, s.out_ch_no + length(s.iio_dev_cfg.mon_ch));


%% Button Setting
figure('Name','TX','NumberTitle','off');
TransmittingDisplay = uicontrol('Style', 'text', 'Position',[55,150,155,35],'String', 'Transmitting','FontSize',20,'HorizontalAlignment','left','BackgroundColor',[0.937 0.867 0.867]);
button = uicontrol; % Generate GUI button
set(button,'String','Stop !','Position',[80 50 100 60]); % Add "Stop !" text
set(gcf,'Units','centimeters','position',[3 3 7 6]); % Set the postion of GUI

state = 1; % status Start
Run_time_number = 1;
%% Main
switch Mode
    case 'step'
        while(state == 1)
        %  try
           %step(tx_object,TX_Hardware);
           input{1} = real(TX_Hardware(1:Len));
           input{2} = imag(TX_Hardware(1:Len));                              
           output = stepImpl(s, input);
           rssi1 = output{s.out_ch_no+1};    
           %[data_rx_raw, dataLength, lostSample] = step(rx_object);
           data_rx_raw = complex(output{1},output{2});
    
           if Run_time_number > Ready_Time
        
                % ----- RX Raw -----%
                data_rx_scaled = double(data_rx_raw)./scale; % [3000x1]
                RX = data_rx_scaled.'; % [1x3000]
        
                subplot(2,4,1),plot(RX,'.');title('RX-Raw');axis([-1.5 1.5 -1.5 1.5]);axis square;
                subplot(2,4,2),plot(real(RX));title('I');axis([1 3000 -1.5 1.5]);axis square;
                subplot(2,4,3),plot(imag(RX));title('Q');axis([1 3000 -1.5 1.5]);axis square;
        
                [Spectrum_waveform,Welch_Spectrum_frequency] = pwelch(RX,[],[],[],Fs,'centered','power');
                subplot(2,4,4),plot(Welch_Spectrum_frequency,pow2db(Spectrum_waveform));
                title('Welch Power Spectral Density');axis([-Fs/2 Fs/2 -100 -10]);axis square;
        
                drawnow;
        
                % ----- Demodulation -----%
                [M_n,Threshold_graph,H_est_time,RX_Payload_1_no_Equalizer,RX_Payload_2_no_Equalizer,RX_Payload_1_no_pilot,RX_Payload_2_no_pilot,BER] = OFDM_RX(RX,Parameters_struct);
                subplot(2,4,5),plot(1:length(M_n),M_n,1:length(M_n),Threshold_graph);title('Packet Detection');axis([1,length(M_n),0,1.2]);axis square;
                subplot(2,4,6),plot(abs(H_est_time));title('Channel Estimation');axis([1 64 0 4]);axis square;xlabel('Time');

                subplot(2,4,7),plot([RX_Payload_1_no_Equalizer,RX_Payload_2_no_Equalizer],'*');
                title('Before Equalizer');axis([-18 18 -18 18]);axis square;
        
                subplot(2,4,8),plot([RX_Payload_1_no_pilot,RX_Payload_2_no_pilot],'*');
                title({'Demodulation';['BER = ',num2str(BER)]});axis([-5 5 -5 5]);axis square;
        
                Run_time_number = Run_time_number+1;
            end % Start
    
            if Run_time_number <= Ready_Time  % Ready
                % disp('Ready');
            end
            Run_time_number = Run_time_number+1;
    
            % ----- Button Behavior -----%
            %set(button,'Callback','setstate0_RX'); % Set the reaction of pushing button
    
            %catch
            %    ErrorMessage = lasterr;
            %    fprintf('Error Message : \n');
            %    disp(ErrorMessage);
            %    fprintf(2,'Error occurred & Stop Hardware\n');
        
             

            %end % Error control
           
           % ----- Button Behavior -----%
           set(button,'Callback','setstate0_TX'); % Set the reaction of pushing button
           drawnow;
        end
        s.releaseImpl();

end