clear all
clc
nSamp = 8; 
nSymbol = 20;
nBitPerSymbol = 16; 
%Number of sub carriers is equal to number of bits per symbol
subcarrierIndex = [-26:-1 1:26];
fsMHz = 20;
nFFTSize=60;
M = 4; 
%M represents M of M-ary PSK; m=4 for QPSK
numSymBits = nSymbol*nBitPerSymbol;

msg_orig = randsrc(1,numSymBits,[0:M-1]); 
%generates a random signal(1-D array) with values 0 to M-1 of length numSymBits
grayencod = bitxor(0:M-1, floor((0:M-1)/2)); 
%generates gray code look up table
msg_gr_orig = grayencod(msg_orig+1); 
%codes the original message using gray encoding
disp(msg_orig);
msg_tx = pskmod(msg_gr_orig,M); 
%modulates the data using M-ary PSK
y=reshape(msg_tx,nBitPerSymbol,nSymbol);
st = []; 
%st will contain the OFDM signal to be transmitted
U=msg_tx.';


for j=1:nSymbol
    input = zeros(1,nFFTSize);
    input((subcarrierIndex+(nFFTSize/2))+1) = U(j,:); 
    %assigns the modulated message bits to input array
    input1 = fftshift(input); 
    %moves zero frequency component to center of the array
    temp=ifft(input1,nFFTSize); 
    %performs ifft on the modulated signal to generate OFDM symbol
    temp=[temp(57:64) temp temp(1:8)]; 
    %appends cyclic prefix to both ends of the OFDM symbol
    st = [st temp]; 
    %appends the symbol to the signal to be transmitted
end

[Pxx,W] = pwelch(st,[],[],4096,fsMHz); 
%calculates the power density function(Pxx) of the signal to be transmitted and the frequency vector W
plot([-2048:2047]*fsMHz/4096,10*log10(fftshift(Pxx)),'b'); 
%plots the PSD of signal in dB after shifting the zero frequency component to the center.
xlabel('frequency, MHz')
ylabel('power spectral density')
title('Transmit spectrum OFDM based on 802.11a with 20% guard band  ');
hold on