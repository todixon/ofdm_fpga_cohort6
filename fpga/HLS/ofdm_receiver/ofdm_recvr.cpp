#include "fft.h"

static unsigned short count;
static DTYPE xr[ SIZE ];
static DTYPE xi[ SIZE ];
static DTYPE xr_out[ SIZE ];
static DTYPE xi_out[ SIZE ];
static int   dout[ SIZE ];

/* void ofdm_receiver( volatile DTYPE *inptr, volatile uint32_t *outptr )
{
#pragma AP interface ap_fifo port=inptr
#pragma AP interface ap_fifo port=outptr
#pragma AP interface ap_ctrl_none port=return

	*outptr++ = dout[ count ];

	xr[ count ] = *inptr++;
	xi[ count ] = *inptr++;
	count++;
	if( count == 1024 ){
		count = 0;
		demod( xr, xi, dout, xr_out, xi_out );
	}
} */

void xillybus_wrapper(int *in, int *out) { 
#pragma AP interface ap_fifo port=in
#pragma AP interface ap_fifo port=out
#pragma AP interface ap_ctrl_none port=return 
  int tempin1;
  int tempin2;

  tempin1 = *in++;
  tempin2 = *in++; // make sure we read the 2nd input value (imaginary part) before calculating
  *out++ = dout[ count ];
  
  xr[ count ] = *((DTYPE *) &tempin1);
  xi[ count ] = *((DTYPE *) &tempin2);
  count++;
  if( count == 1024 ){
#pragma HLS reset variable=count
		demod( xr, xi, dout, xr_out, xi_out );
	}
}