#!/usr/bin/env python
#
# Copyright (C) 2015 Analog Devices, Inc.
# Author: Paul Cercueil <paul.cercueil@analog.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

import iio
import pickle
import array
from sys import argv
import matplotlib.pyplot as plt
import numpy as np
import math
import scipy.io as sio
from scipy import signal

class fmcomms4(object):		
	def __init__(self,uri):
		print("Init FMCOMMS4.")
		ctx = iio.Context(uri)
		if uri is not None:
			print('Using auto-detected IIO context at URI \"%s\"' % uri)

		print('IIO context created: ' + ctx.name)
		print('Backend version: %u.%u (git tag: %s)' % ctx.version)
		print('Backend description string: ' + ctx.description)
		
		if len(ctx.attrs) > 0:
			print('IIO context has %u attributes:' % len(ctx.attrs))
		for attr, value in ctx.attrs.items():
			print('\t' + attr + ': ' + value)

		print('IIO context has %u devices:' % len(ctx.devices))

		control_device = ctx.find_device('ad9361-phy')
		print("control_device :" + control_device.name)		

		#Set TX attenuation to 0
		#control_device.debug_attrs["adi,tx-attenuation-mdB"].value = "0.0"
		#print("%s:tx attenuation: %s" % (control_device.name, control_device.debug_attrs["adi,tx-attenuation-mdB"]))


		# Set the Radio Carrier Frequency
		control_chan_rx = control_device.find_channel('altvoltage0',True)	
		control_chan_tx = control_device.find_channel('altvoltage1',True)	
		control_chan_rx.attrs["frequency"].value = "2200000000"
		control_chan_tx.attrs["frequency"].value = "2200000000"
		print("%s: Freq:%s"%(control_chan_rx.id , control_chan_rx.attrs["frequency"].value))
		print("%s: Freq:%s"%(control_chan_tx.id , control_chan_tx.attrs["frequency"].value))

		# Set the Radio channel Sampling rate and RF bandwidth
		control_chan_rx = control_device.find_channel('voltage0',False)	
		control_chan_tx = control_device.find_channel('voltage0',True)	
		
		control_chan_rx.attrs["sampling_frequency"].value = "20000000"
		control_chan_rx.attrs["rf_bandwidth"].value = "20000000"
		control_chan_rx.attrs["gain_control_mode"].value = "manual"
		control_chan_rx.attrs["rf_port_select"] = "A_BALANCED"
		control_chan_rx.attrs["hardwaregain"] = "64"

		control_chan_tx.attrs["sampling_frequency"].value = "20000000"
		control_chan_tx.attrs["rf_bandwidth"].value = "20000000"	
		control_chan_tx.attrs["rf_port_select"].value = "A"
		control_chan_tx.attrs["hardwaregain"].value = "0"
			
		# Find RX and TX data devices
		self.tx_device = ctx.find_device('cf-ad9361-dds-core-lpc')		
		self.rx_device = ctx.find_device('cf-ad9361-lpc')
		
		#Find 2 RX and TX data channel		
		self.rx_data_chan_0 = self.rx_device.find_channel('voltage0',False)
		self.rx_data_chan_1 = self.rx_device.find_channel('voltage1',False)
		
		self.tx_data_chan_0 = self.tx_device.find_channel('voltage0',True)		
		self.tx_data_chan_1 = self.tx_device.find_channel('voltage1',True)

		self.tx_data_chan_2 = self.rx_device.find_channel('altvoltage0',True)
		for enable in [False,True] :
			print("Turning channel on/off: %d"%enable)
			self.rx_data_chan_0.enabled = enable
			self.rx_data_chan_1.enabled = enable
			self.tx_data_chan_0.enabled = enable
			self.tx_data_chan_1.enabled = enable

		print("tx_device :" + self.tx_device.name)
		print("sample size:%d" % self.tx_device.sample_size)
		print("rx_device :" + self.rx_device.name)
		print("sample size:%d" % self.rx_device.sample_size)

		self.ctx = ctx

	
	def write_file(self, data):
		dfile = open('txdata.txt','w')
		for d in data :
			dfile.write("%d\n"%d)
	
	def transmit_data(self,data_i, data_q):
		tx_buff = iio.Buffer(self.tx_device,2048,False)
		#self.tx_data_chan_1.attrs["raw"] = 0
		print ("transmit_data len=%d" % (len(data_i)))
		data = array.array('i',(0 for i in range(0,len(data_i)*2)))

		for i in range(0,len(data_i)):
			data[i*2] = data_i[i]
			data[i*2+1] = data_q[i]
			#print("%d:%d,%d"%(i,di,dq))
		#data_array = bytearray(data)
		#print len(data_array)
	        samples =  tx_buff.write(data)
		#self.write_file(data)		
		#print("Pushing %d samples to buffer."%samples)
		n_tx_size = tx_buff.push(samples)
		print("Samples:%d, Pushed size: %d bytes."%(samples,n_tx_size))
		#tx_buff.destroy()
			

def plot_signal(x):
	fs = 20000000
	f, Pxx_den = signal.welch(x, fs, nperseg=len(x))
	#print f, Pxx_den
	plt.semilogy(f, Pxx_den)
	#plt.ylim([0.5e-3, 1])
	plt.xlabel('frequency [Hz]')
	plt.ylabel('PSD [V**2/Hz]')
	plt.show()

def main():
	print('Library version: %u.%u (git tag: %s)' % iio.version)

	if len(argv) == 3 and argv[1] == '--uri':
		uri = argv[2]
	else:
		contexts = iio.scan_contexts()
		if len(contexts) > 1:
			print('Multiple contexts found. Please select one using --uri:')
			for index, each in enumerate(contexts):
				print('\t%d: %s [%s]' % (index, contexts[each], each))
			return

		uri = next(iter(contexts), None)

	print("uri:%s",uri)
	comm = fmcomms4(uri)
	
	mat_contents = sio.loadmat('TX_signal.mat')
	
	TX_signal = mat_contents['TX_signal']
	#print TX_signal
	tx = 10000.0*np.array(TX_signal[0]) + 0.5
	#plot_signal(tx)
	
	tx_real_int = tx.real.astype(int)
	tx_imag_int = tx.imag.astype(int)
	print ("TX len=%d" % len(tx_imag_int))	
	for count in range(1,5000):
		print ("transimitting %d Times"%count)
		comm.transmit_data(tx_real_int, tx_imag_int)
	


if __name__ == '__main__':
	main()
