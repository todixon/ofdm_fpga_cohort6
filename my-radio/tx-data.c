/*
 * libiio - AD9361 IIO streaming example
 *
 * Copyright (C) 2014 IABG mbH
 * Author: Michael Feilen <feilen_at_iabg.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 **/

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include "fftw3.h"
#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <iio.h>



#define N_FFT  64
#define REAL   0
#define IMAG   1


static void swap(fftw_complex *v1, fftw_complex *v2)
{
    fftw_complex tmp;
    memcpy(&tmp,v1,sizeof(fftw_complex));
    //*v1 = *v2;
    memcpy(v1,v2,sizeof(fftw_complex));
    // *v2 = tmp;
    memcpy(v2,&tmp, sizeof(fftw_complex));
}

static void fftshift(fftw_complex *data, int count)
{
    int k = 0;
    int c = (int) (count/2);
    // For odd and for even numbers of element use different algorithm
    if (count % 2 == 0)
    {
        
    }
}

static void ifftshift(fftw_complex *data, int count)
{
    int k = 0;
    int c = (int) (count/2);
    if (count % 2 == 0)
    {
        for (k = 0; k < c; k++) {
            //printf("swap %u, %u.\n",k,k+c);
            swap(&data[k], &data[k+c]);
        }
    }
}

static void log_array(fftw_complex *data, int count)
{
    #if 0
    for (int i = 0; i< count; i++) 
	{
		printf("%u: %2.5f + %2.5fj \n",i+1, data[i][REAL], data[i][IMAG]);
	}
    #endif
}

//%% Short_preamble
//S_k = sqrt(13/6)*[0,0,1+j,0,0,0,-1-j,0,0,0,1+j,0,0,0,-1-j,0,0,0,-1-j,0,0,0,1+j,0,0,0,0,0,0,0,-1-j,0,0,0,-1-j,0,0,0,1+j,0,0,0,1+j,0,0,0,1+j,0,0,0,1+j,0,0]; // [1x53]
//virtual_subcarrier = zeros(1,N_FFT-length(S_k)); % [1x11]
int sk_real[] = {0,0,1,0,0,0,-1,0,0,0,1,0,0,0,-1,0,0,0,-1,0,0,0,1,0,0,0,0,0,0,0,-1,0,0,0,-1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0};
int sk_imag[] = {0,0,1,0,0,0,-1,0,0,0,1,0,0,0,-1,0,0,0,-1,0,0,0,1,0,0,0,0,0,0,0,-1,0,0,0,-1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0};
float amp_sk;
fftw_complex spreamble_slot_Frequency[N_FFT];
fftw_complex spreamble_slot_Time[N_FFT];
fftw_complex long_preamble_slot_Time[N_FFT];
fftw_plan ifft;
float RRC_filter[] = {0.0021,   -0.0106,    0.0300,   -0.0531,   -0.0750,    0.4092,    0.8038,    0.4092,   -0.0750,   -0.0531,    0.0300,   -0.0106,    0.0021};

#define FRAME_LEN 972
fftw_complex frame[960];
fftw_complex tx_frame[FRAME_LEN];

int init_tx(fftw_complex *short_preamble, fftw_complex *long_preamble)
{
	amp_sk = sqrt(13.0/6.0);
	//Short_preamble_slot_Frequency = [virtual_subcarrier(1:6),S_k,virtual_subcarrier(7:11)]; % [1x64]
	int k=0;
	for (int i=0;i<6; i++) {
		spreamble_slot_Frequency[i][REAL] = 0;
		spreamble_slot_Frequency[i][IMAG] = 0;
	}
	for (int i=0;i<53; i++) {
		spreamble_slot_Frequency[6+i][REAL] = amp_sk*sk_real[i];
		spreamble_slot_Frequency[6+i][IMAG] = amp_sk*sk_imag[i];
	}
	for (int i=59;i<64; i++) {
		spreamble_slot_Frequency[i][REAL] = 0;
		spreamble_slot_Frequency[i][IMAG] = 0;
	}
    log_array(spreamble_slot_Frequency,N_FFT);
	printf("====================\n");

	//Short_preamble_slot_Time = ifft(ifftshift(Short_preamble_slot_Frequency)); % [1x64]
    ifftshift(spreamble_slot_Frequency,N_FFT);
	ifft = fftw_plan_dft_1d(N_FFT, spreamble_slot_Frequency, spreamble_slot_Time, FFTW_BACKWARD, FFTW_ESTIMATE);   //Setup fftw plan for ifft
	fftw_execute(ifft);
	
	
    log_array(spreamble_slot_Frequency,N_FFT);
	printf("====================\n");
	for (int i = 0; i< N_FFT; i++) 
	{
		spreamble_slot_Time[i][REAL]/=N_FFT; 
		spreamble_slot_Time[i][IMAG]/=N_FFT;		
	}
    log_array(spreamble_slot_Time,N_FFT);
    //Short_preamble = repmat(Short_preamble_slot_Time(1:16),1,10); % [1x160]
    for (int i = 0; i<10; i++) {
        for (int j= 0; j<16; j++) {
            short_preamble[i*16+j][REAL] = spreamble_slot_Time[j][REAL];
            short_preamble[i*16+j][IMAG] = spreamble_slot_Time[j][IMAG];
        }
    }
    log_array(short_preamble,160);

//%% Long_preamble
    //int L_k[] = {1,1,-1,-1,1,1,-1,1,-1,1,1,1,1,1,1,-1,-1,1,1,-1,1,-1,1,1,1,1,0,1,-1,-1,1,1,-1,1,-1,1,-1,-1,-1,-1,-1,1,1,-1,-1,1,-1,1,-1,1,1,1,1}; //% [1x53]
    int L_k[] = {0,0,0,0,0,0,1,1,-1,-1,1,1,-1,1,-1,1,1,1,1,1,1,-1,-1,1,1,-1,1,-1,1,1,1,1,0,1,-1,-1,1,1,-1,1,-1,1,-1,-1,-1,-1,-1,1,1,-1,-1,1,-1,1,-1,1,1,1,1,0,0,0,0,0};//[virtual_subcarrier(1:6),L_k,virtual_subcarrier(7:11)]; % [1x64]
    fftw_complex long_preamble_slot_Frequency[N_FFT];
    for (int i=0; i<N_FFT; i++) {
        long_preamble_slot_Frequency[i][REAL] = (float)L_k[i];
        long_preamble_slot_Frequency[i][IMAG] = 0.0;
    }

    ifftshift(long_preamble_slot_Frequency,N_FFT);
    ifft = fftw_plan_dft_1d(N_FFT, long_preamble_slot_Frequency, long_preamble_slot_Time, FFTW_BACKWARD, FFTW_ESTIMATE);   //Setup fftw plan for ifft % [1x64]
    fftw_execute(ifft);
    //long_preamble = [Long_preamble_slot_Time(33:64),Long_preamble_slot_Time,Long_preamble_slot_Time]; % [1x160]
    memcpy(&long_preamble[0], &long_preamble_slot_Time[32],sizeof(fftw_complex)*32);
    memcpy(&long_preamble[32], &long_preamble_slot_Time[0],sizeof(fftw_complex)*64);
    memcpy(&long_preamble[96], &long_preamble_slot_Time[0],sizeof(fftw_complex)*64);
    log_array(long_preamble,160);

	return 0;
}

fftw_complex qam16_table[] = 
{   {-3,3},  //0000
    {-3,1},  //0001
    {-3,-3},  //0010
    {-3,-1},  //0011
    {-1,3},  //0100
    {-1,1},  //0101
    {-1,-3},  //0110
    {-1,-1},  //0111
     {3,3},  //1000
    {3,1},  //1001
    {3,-3},  //1010
    {3,-1},  //1011
    {1,3},  //1100
    {1,1},  //1101
    {1,-3},  //1110
    {1,-1},  //1111
};

int qam16_encode(unsigned char bits, fftw_complex *symbol)
{
    int idx = (bits & 0xf);
    *symbol[REAL] = qam16_table[idx][REAL];
    *symbol[IMAG] = qam16_table[idx][IMAG];
    return 0;
}

fftw_complex data_slot_time[48];
fftw_complex data_slot_freq[64];
int payload_encode(unsigned char *data,fftw_complex *symbols)
{
    fftw_complex *symbols_start = &symbols[16]; //after cyclic prefix
    int i=0, k=0;
    for (i = 0; i< 24; i++)
    {
        qam16_encode(data[i], &data_slot_time[k]);
        k++;        
        qam16_encode(data[i]>>4, &data_slot_time[k]);
        k++;
    }
    //0 :virtual_subcarrier(1:6),
    //6: data1_slot_Frequency(1:5),
    //11: pilot(1),
    //12: data1_slot_Frequency(6:18),
    //25: pilot(2),
    //26: data1_slot_Frequency(19:24),
    //32: 0,
    //33: data1_slot_Frequency(25:30),
    //39: pilot(3),
    //40: data1_slot_Frequency(31:43),
    //53: pilot(4),
    //54: data1_slot_Frequency(44:48),
    //59: virtual_subcarrier(7:11)]; % [1x64]

    memset(data_slot_freq, 0, sizeof(fftw_complex)*N_FFT);
    data_slot_freq[11][REAL] = 1;  //pilot
    data_slot_freq[25][REAL] = 1;  //pilot
    data_slot_freq[39][REAL] = 1;  //pilot
    data_slot_freq[53][REAL] = -1;  //pilot
    memcpy(&data_slot_freq[6], &data_slot_time[0], sizeof(fftw_complex)*5);
    memcpy(&data_slot_freq[12], &data_slot_time[5], sizeof(fftw_complex)*13);
    memcpy(&data_slot_freq[26], &data_slot_time[18], sizeof(fftw_complex)*6);
    memcpy(&data_slot_freq[33], &data_slot_time[24], sizeof(fftw_complex)*6);
    memcpy(&data_slot_freq[40], &data_slot_time[30], sizeof(fftw_complex)*13);
    memcpy(&data_slot_freq[54], &data_slot_time[43], sizeof(fftw_complex)*5);


	//Short_preamble_slot_Time = ifft(ifftshift(Short_preamble_slot_Frequency)); % [1x64]
    ifftshift(spreamble_slot_Frequency,N_FFT);
	ifft = fftw_plan_dft_1d(N_FFT, data_slot_freq, symbols_start, FFTW_BACKWARD, FFTW_ESTIMATE);   //Setup fftw plan for ifft
	fftw_execute(ifft);

    //Add cyclic previx
    memcpy(symbols,symbols_start, sizeof(fftw_complex)*16);
    return k;
}
#define min(a,b) ( ((a)<(b))?(a):(b))
void rrc_conv(fftw_complex *input, fftw_complex *output ,int len)
{
    int sum = 0;
    int rrc_len = sizeof(RRC_filter)/sizeof(fftw_complex); //rrc coeff len
    int rrc_end = rrc_len - 1; //last rrc coeff
    for (int i = 0 ; i< len + rrc_end; i++)
    {
       output[i][REAL] = 0;
       output[i][IMAG] = 0;
       for (int j = 0; j< min(i+1,rrc_len); j++) {            
            int k = rrc_end - j; 
            output[i][REAL] += RRC_filter[rrc_len-j] * input[i+j][REAL];
            output[i][REAL] += RRC_filter[rrc_len-j] * input[i+j][REAL];
       }
    }
}


/* helper macros */
#define MHZ(x) ((long long)(x*1000000.0 + .5))
#define GHZ(x) ((long long)(x*1000000000.0 + .5))

#define ASSERT(expr) { \
	if (!(expr)) { \
		(void) fprintf(stderr, "assertion failed (%s:%d)\n", __FILE__, __LINE__); \
		(void) abort(); \
	} \
}

/* RX is input, TX is output */
enum iodev { RX, TX };

/* common RX and TX streaming params */
struct stream_cfg {
	long long bw_hz; // Analog banwidth in Hz
	long long fs_hz; // Baseband sample rate in Hz
	long long lo_hz; // Local oscillator frequency in Hz
	const char* rfport; // Port name
};

/* static scratch mem for strings */
static char tmpstr[64];

/* IIO structs required for streaming */
static struct iio_context *ctx   = NULL;
static struct iio_channel *rx0_i = NULL;
static struct iio_channel *rx0_q = NULL;
static struct iio_channel *tx0_i = NULL;
static struct iio_channel *tx0_q = NULL;
static struct iio_channel *tx0_i_a = NULL;
static struct iio_buffer  *rxbuf = NULL;
static struct iio_buffer  *txbuf = NULL;

static bool stop;

/* cleanup and exit */
static void shutdown()
{
	printf("* Destroying buffers\n");
	if (rxbuf) { iio_buffer_destroy(rxbuf); }
	if (txbuf) { iio_buffer_destroy(txbuf); }

	printf("* Disabling streaming channels\n");
	if (rx0_i) { iio_channel_disable(rx0_i); }
	if (rx0_q) { iio_channel_disable(rx0_q); }
	if (tx0_i) { iio_channel_disable(tx0_i); }
	if (tx0_q) { iio_channel_disable(tx0_q); }

	printf("* Destroying context\n");
	if (ctx) { iio_context_destroy(ctx); }
	exit(0);
}

static void handle_sig(int sig)
{
	printf("Waiting for process to finish...\n");
	stop = true;
}

/* check return value of attr_write function */
static void errchk(int v, const char* what) {
	 if (v < 0) { fprintf(stderr, "Error %d writing to channel \"%s\"\nvalue may not be supported.\n", v, what); shutdown(); }
}

/* write attribute: long long int */
static void wr_ch_lli(struct iio_channel *chn, const char* what, long long val)
{
	errchk(iio_channel_attr_write_longlong(chn, what, val), what);
}

/* write attribute: string */
static void wr_ch_str(struct iio_channel *chn, const char* what, const char* str)
{
	errchk(iio_channel_attr_write(chn, what, str), what);
}

/* helper function generating channel names */
static char* get_ch_name(const char* type, int id)
{
	snprintf(tmpstr, sizeof(tmpstr), "%s%d", type, id);
	return tmpstr;
}

/* returns ad9361 phy device */
static struct iio_device* get_ad9361_phy(struct iio_context *ctx)
{
	struct iio_device *dev =  iio_context_find_device(ctx, "ad9361-phy");
	ASSERT(dev && "No ad9361-phy found");
	return dev;
}

/* finds AD9361 streaming IIO devices */
static bool get_ad9361_stream_dev(struct iio_context *ctx, enum iodev d, struct iio_device **dev)
{
	switch (d) {
	case TX: *dev = iio_context_find_device(ctx, "cf-ad9361-dds-core-lpc"); return *dev != NULL;
	case RX: *dev = iio_context_find_device(ctx, "cf-ad9361-lpc");  return *dev != NULL;
	default: ASSERT(0); return false;
	}
}

/* finds AD9361 streaming IIO channels */
static bool get_ad9361_stream_ch(struct iio_context *ctx, enum iodev d, struct iio_device *dev, int chid, struct iio_channel **chn, bool isAlt)
{
	if (!isAlt)
		*chn = iio_device_find_channel(dev, get_ch_name("voltage", chid), d == TX);
	if ( (!*chn) || isAlt )
		*chn = iio_device_find_channel(dev, get_ch_name("altvoltage", chid), d == TX);
	return *chn != NULL;
}

/* finds AD9361 phy IIO configuration channel with id chid */
static bool get_phy_chan(struct iio_context *ctx, enum iodev d, int chid, struct iio_channel **chn)
{
	switch (d) {
	case RX: *chn = iio_device_find_channel(get_ad9361_phy(ctx), get_ch_name("voltage", chid), false); return *chn != NULL;
	case TX: *chn = iio_device_find_channel(get_ad9361_phy(ctx), get_ch_name("voltage", chid), true);  return *chn != NULL;
	default: ASSERT(0); return false;
	}
}

/* finds AD9361 local oscillator IIO configuration channels */
static bool get_lo_chan(struct iio_context *ctx, enum iodev d, struct iio_channel **chn)
{
	switch (d) {
	 // LO chan is always output, i.e. true
	case RX: *chn = iio_device_find_channel(get_ad9361_phy(ctx), get_ch_name("altvoltage", 0), true); return *chn != NULL;
	case TX: *chn = iio_device_find_channel(get_ad9361_phy(ctx), get_ch_name("altvoltage", 1), true); return *chn != NULL;
	default: ASSERT(0); return false;
	}
}

/* applies streaming configuration through IIO */
bool cfg_ad9361_streaming_ch(struct iio_context *ctx, struct stream_cfg *cfg, enum iodev type, int chid)
{
	struct iio_channel *chn = NULL;

	// Configure phy and lo channels
	printf("* Acquiring AD9361 phy channel %d\n", chid);
	if (!get_phy_chan(ctx, type, chid, &chn)) {	return false; }
	wr_ch_str(chn, "rf_port_select",     cfg->rfport);
	wr_ch_lli(chn, "rf_bandwidth",       cfg->bw_hz);
	wr_ch_lli(chn, "sampling_frequency", cfg->fs_hz);
	if (type == TX) {
		wr_ch_lli(chn, "hardwaregain", 0); //set tx attenuation to 0
	}

	// Configure LO channel
	printf("* Acquiring AD9361 %s lo channel\n", type == TX ? "TX" : "RX");
	if (!get_lo_chan(ctx, type, &chn)) { return false; }
	wr_ch_lli(chn, "frequency", cfg->lo_hz);
	return true;
}

/* simple configuration and streaming */
int main (int argc, char **argv)
{
   int i,ret;
   unsigned char *data = "OFDM is a form of multicarrier modulation. An OFDM signal consists of a number of closely spaced modulated carriers. When modulation of any form - voice, data, etc. is applied to a carrier, then sidebands spread out either side. It is necessary for a receiver to be able to receive the whole signal to be able to successfully demodulate the data. As a result when signals are transmitted close to one another they must be spaced so that the receiver can separate them using a filter and there must be a guard band between them. This is not the case with OFDM. Although the sidebands from each carrier overlap, they can still be received without the interference that might be expected because they are orthogonal to each another. This is achieved by having the carrier spacing equal to the reciprocal of the symbol period,To see how OFDM works, it is necessary to look at the receiver. This acts as a bank of demodulators, translating each carrier down to DC. The resulting signal is integrated over the symbol period to regenerate the data from that carrier. The same demodulator also demodulates the other carriers. As the carrier spacing equal to the reciprocal of the symbol period means that they will have a whole number of cycles in the symbol period and their contribution will sum to zero - in other words there is no interference contribution.";
   fftw_complex *short_preamble = &frame[0];
   fftw_complex *long_preamble = &frame[160];
   ret = init_tx(short_preamble, long_preamble);

	// Streaming devices
	struct iio_device *tx;
	struct iio_device *rx;

	// RX and TX sample counters
	size_t nrx = 0;
	size_t ntx = 0;

	// Stream configurations
	struct stream_cfg rxcfg;
	struct stream_cfg txcfg;

	// Listen to ctrl+c and ASSERT
	signal(SIGINT, handle_sig);

	// RX stream config
	rxcfg.bw_hz = MHZ(20);   // 20 MHz rf bandwidth
	rxcfg.fs_hz = MHZ(20);   // 20 MS/s rx sample rate
	rxcfg.lo_hz = GHZ(2.2); // 2 GHz rf frequency
	rxcfg.rfport = "A_BALANCED"; // port A (select for rf freq.)

	// TX stream config
	txcfg.bw_hz = MHZ(20); // 20 MHz rf bandwidth
	txcfg.fs_hz = MHZ(20);   //20 MS/s tx sample rate
	txcfg.lo_hz = GHZ(2.2); // 2 GHz rf frequency
	txcfg.rfport = "A"; // port A (select for rf freq.)

	printf("* Acquiring IIO context\n");
	if (argc > 1) {
		printf("Create from uri%s\n",argv[1]);
		ASSERT((ctx = iio_create_context_from_uri(argv[1])) && "No context");
	}else {
		printf("Create from local\n");
		ASSERT((ctx = iio_create_local_context()) && "No context");
	}
	ASSERT(iio_context_get_devices_count(ctx) > 0 && "No devices");

	printf("* Acquiring AD9361 streaming devices\n");
	ASSERT(get_ad9361_stream_dev(ctx, TX, &tx) && "No tx dev found");
	ASSERT(get_ad9361_stream_dev(ctx, RX, &rx) && "No rx dev found");

	printf("* Configuring AD9361 for streaming\n");
	ASSERT(cfg_ad9361_streaming_ch(ctx, &rxcfg, RX, 0) && "RX port 0 not found");
	ASSERT(cfg_ad9361_streaming_ch(ctx, &txcfg, TX, 0) && "TX port 0 not found");

	printf("* Initializing AD9361 IIO streaming channels\n");
	ASSERT(get_ad9361_stream_ch(ctx, RX, rx, 0, &rx0_i,false) && "RX chan i not found");
	ASSERT(get_ad9361_stream_ch(ctx, RX, rx, 1, &rx0_q,false) && "RX chan q not found");
	ASSERT(get_ad9361_stream_ch(ctx, TX, tx, 0, &tx0_i,false) && "TX chan i not found");
	ASSERT(get_ad9361_stream_ch(ctx, TX, tx, 1, &tx0_q,false) && "TX chan q not found");
	ASSERT(get_ad9361_stream_ch(ctx, TX, tx, 0, &tx0_i_a,true) && "TX chan i not found");
	//wr_ch_lli(tx0_i_a,"raw",0);

	printf("* Enabling IIO streaming channels\n");
	iio_channel_enable(rx0_i);
	iio_channel_enable(rx0_q);
	iio_channel_enable(tx0_i);
	iio_channel_enable(tx0_q);

	printf("* Creating non-cyclic IIO buffers with 1 MiS\n");
	/*
	rxbuf = iio_device_create_buffer(rx, 1024*1024, false);
	if (!rxbuf) {
		perror("Could not create RX buffer");
		shutdown();
	}*/
	txbuf = iio_device_create_buffer(tx, 2048, false);
	if (!txbuf) {
		perror("Could not create TX buffer");
		shutdown();
	}
	//int16_t data[2048];
	//int data_len = read_file("txdata.txt", data);

	while (!stop)
	{
		ssize_t nbytes_rx, nbytes_tx;
		char *p_dat, *p_end;
		ptrdiff_t p_inc;		
    
    for (i = 0; i< strlen(data); i+=48)  // 48 bytes can be transmitted every frame.
    {
       int k = 0;
       payload_encode(&data[i] , &frame[320]);  //320
       payload_encode(&data[i] , &frame[400]);  //320+80
   
       //%% Oversampling
       memcpy(&frame[480], &frame[0], 480*sizeof(fftw_complex));
       //%%RRC
       rrc_conv(frame, tx_frame,960);
    	printf("data_len = %d * Starting IO streaming (press CTRL+C to cancel)\n",sizeof(tx_frame));
		#if 0
		// Refill RX buffer
		nbytes_rx = iio_buffer_refill(rxbuf);
		if (nbytes_rx < 0) { printf("Error refilling buf %d\n",(int) nbytes_rx); shutdown(); }

		// READ: Get pointers to RX buf and read IQ from RX buf port 0
		p_inc = iio_buffer_step(rxbuf);
		p_end = iio_buffer_end(rxbuf);
		for (p_dat = (char *)iio_buffer_first(rxbuf, rx0_i); p_dat < p_end; p_dat += p_inc) {
			// Example: swap I and Q
			const int16_t i = ((int16_t*)p_dat)[0]; // Real (I)
			const int16_t q = ((int16_t*)p_dat)[1]; // Imag (Q)
			((int16_t*)p_dat)[0] = q;
			((int16_t*)p_dat)[1] = i;
		}
		#endif
		// WRITE: Get pointers to TX buf and write IQ to TX buf port 0
		p_inc = iio_buffer_step(txbuf);
		p_end = iio_buffer_end(txbuf);
		for (p_dat = (char *)iio_buffer_first(txbuf, tx0_i); p_dat < p_end; p_dat += p_inc) {
			// Example: fill with zeros
			// 12-bit sample needs to be MSB alligned so shift by 4
			// https://wiki.analog.com/resources/eval/user-guides/ad-fmcomms2-ebz/software/basic_iq_datafiles#binary_format
			if (k<= 972) {
			    ((int16_t*)p_dat)[0] = 10000*tx_frame[k][REAL] ; // Real (I)
			    ((int16_t*)p_dat)[1] = 10000*tx_frame[k][IMAG] ; // Imag (Q)
            }else {
                ((int16_t*)p_dat)[0] = 0 ; // Real (I)
			    ((int16_t*)p_dat)[1] = 0 ; // Imag (Q)
            }
			//printf("[%d,%d].\n", ((int16_t*)p_dat)[0],((int16_t*)p_dat)[1]);
			//	printf(".");
			
		}
		// Schedule TX buffer
		nbytes_tx = iio_buffer_push(txbuf);
		if (nbytes_tx < 0) { printf("Error pushing buf %d\n", (int) nbytes_tx); shutdown(); }
		// Sample counter increment and status output
		nrx += nbytes_rx / iio_device_get_sample_size(rx);
		ntx += nbytes_tx / iio_device_get_sample_size(tx);
		//printf("\tRX %8.2f MSmp, TX %8.2f MSmp\n", nrx/1e6, ntx/1e6);
		//usleep(500);
       }
	}

	shutdown();


    fftw_destroy_plan(ifft);
    fftw_cleanup();

}
